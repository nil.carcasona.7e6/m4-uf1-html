<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:template match = "/evaluacion">

        <html>
            <body>
            <table>

                <tr>
                    <th><h2>Foto</h2></th>
                    <th><h2>Nombre</h2></th>
                    <th><h2>Apellidos</h2></th>
                    <th><h2>Telefono</h2></th>
                    <th><h2>Repetidor</h2></th>
                    <th><h2>Nota practica</h2></th>
                    <th><h2>Nota examen</h2></th>
                    <th><h2>Nota total</h2></th>

                </tr>

                <xsl:for-each select = "alumno">

                    <xsl:sort select = "apellidos"/>

                    <tr>

                        <th><img>
                            <xsl:attribute name = "src">
                                <xsl:value-of select = "foto"/>
                            </xsl:attribute>
                        </img></th>
                        <th><h4><xsl:value-of select = "nombre"/></h4></th>
                        <th><h4><xsl:value-of select = "apellidos"/></h4></th>
                        <th><h4><xsl:value-of select = "telefono"/></h4></th>
                        <th><h4><xsl:value-of select = "@repite"/></h4></th>
                        <xsl:apply-templates select = "notas"></xsl:apply-templates>
                        <!--<th><h4><xsl:value-of select = "(notas/examen + notas/practicas) div 2"/></h4></th>-->
            

                    </tr>

                </xsl:for-each>

                <!--<xsl:apply-templates select = "alumno"/>-->

            </table>
            </body>
        </html>

    </xsl:template>

    <xsl:template match = "alumno">
        <tr>

            <th><h4><xsl:value-of select = "nombre"/></h4></th>
            <th><h4><xsl:value-of select = "apellidos"/></h4></th>
            <th><h4><xsl:value-of select = "telefono"/></h4></th>
            <th><h4><xsl:value-of select = "@repite"/></h4></th>
            <!--<th><h4><xsl:value-of select = "notas/practicas"/></h4></th>
            <th><h4><xsl:value-of select = "notas/examen"/></h4></th>-->
            <xsl:apply-templates select = "notas"/>
            <!--<th><h4><xsl:value-of select = "(notas/examen + notas/practicas) div 2"/></h4></th>-->
            

        </tr>
    </xsl:template>

    <xsl:template match = "notas">

        <xsl:choose>
            <xsl:when test = "practicas &gt;= 8 ">
                <th><h4 style = "color: blue;"><xsl:value-of select = "practicas"/></h4></th>
            </xsl:when>
            <xsl:when test="practicas &lt; 5">
                <th><h4 style = "color: red;"><xsl:value-of select = "practicas"/></h4></th>
            </xsl:when>
            <xsl:otherwise >
                <th><h4><xsl:value-of select = "practicas"/></h4></th>
            </xsl:otherwise>
        </xsl:choose>

        <xsl:choose>
        
            <xsl:when test="examen &gt;= 8">
                <th><h4 style = "color: blue"><xsl:value-of select = "examen"/></h4></th> 
            </xsl:when>
            <xsl:when test="examen &lt; 5">
                <th><h4 style = "color: red"><xsl:value-of select = "examen"/></h4></th> 
            </xsl:when>
            <xsl:otherwise>
                <th><h4><xsl:value-of select = "examen"/></h4></th> 
            </xsl:otherwise>
            

        </xsl:choose>

        <xsl:choose>
        
            <xsl:when test="((examen + practicas) div 2) &gt;= 8">
                <th><h4 style = "color: blue"><xsl:value-of select = "(examen + practicas) div 2"/></h4></th> 
            </xsl:when>
            <xsl:when test="((examen + practicas) div 2) &lt; 5">       
                <th><h4 style = "color: red"><xsl:value-of select = "(examen + practicas) div 2"/></h4></th> 
            </xsl:when>
            <xsl:otherwise>
                <th><h4><xsl:value-of select = "(examen + practicas) div 2"/></h4></th> 
            </xsl:otherwise>
            

        </xsl:choose>




    </xsl:template>
    
</xsl:stylesheet>