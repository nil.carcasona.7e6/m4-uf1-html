<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
        <body>
            <table>
                <tr>
                    <th>dia</th>
                    <th>maxima</th>
                    <th>minima</th>
                    <th>prediccion</th>
                </tr>
                
                <xsl:for-each select="/root/prediccion/dia">
                    
                    <tr>
                        <th> <xsl:value-of select="@fecha"></xsl:value-of></th>
                        <th> <xsl:value-of select = "temperatura/maxima"></xsl:value-of></th>
                        <th> <xsl:value-of select="temperatura/minima"></xsl:value-of></th>
                        <th> <xsl:value-of select="estado_cielo[3]/@descripcion"></xsl:value-of></th>
                    </tr>
                    
                </xsl:for-each>
                
            </table>
            </body>
        </html>
        
        
    </xsl:template>
</xsl:stylesheet>