<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:template match = "/">

        <xsl:variable name = "arbre" >

            <xsl:value-of select = "discos"/>

        </xsl:variable>


        <lista>

            <xsl:for-each select = "discos/disco">

                <disco>
                    <xsl:value-of select = "title"/>
                    <xsl:variable name = "id">
                        <xsl:value-of select = "interpreter/@id"/>
                    </xsl:variable>
                    <xsl:value-of select = "$arbre/group[@id = $id/content]/name"/>

                    
                </disco>

            </xsl:for-each>

        </lista>

    </xsl:template>
    
</xsl:stylesheet>